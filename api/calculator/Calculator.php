<?php
    
require_once("utilities/Format.php");

class Calculator {
   
    // Car Insurance Properties
    private $car_value;
    private $tax_percentage;
    private $installments;
    private $normal_base_price_percentage;
    private $friday_base_price_percentage;
    private $applied_base_percentage;
    private $comission_percentage;
    private $calculation_reuqest_hour;
    private $user_time;
    private $user_day;

    private $format;

    // Constructor with DB
    public function __construct() {

        // initialization of required values
        $this->car_value = 0;
        $this->tax_percentage = 0;
        $this->installments = 1;
        $this->normal_base_price_percentage = 11;
        $this->friday_base_price_percentage = 13;
        $this->applied_base_percentage = 0;
        $this->commission_percentage = 17;
        $this->calculation_reuqest_hour = 1;
    
        $this->format = new Format();
    }

    ### data get/set functions ###
    public function set_car_value($value){
        $this->car_value = $value;
    }

    public function set_tax_percentage($value){
        $this->tax_percentage = $value;
    }

    public function set_installments($value){
        $this->installments = $value;
    }

    public function set_user_time($value){
        $this->user_time = $value;
    }

    public function set_user_day($value){
        $this->user_day = $value;
    }
    
    

    ### FUNCTION TO CALCULATE BASE PRICE ###
    private function calculate_base_price() {
        
        $day = $this->user_day;
        $time_hour = $this->user_time;

        $car_base_price = 0;
        if($day == 5 && $time_hour >= 15 && $time_hour <= 20 ){
            $car_base_price = $this->car_value * ($this->friday_base_price_percentage/100);
            $this->applied_base_percentage = $this->friday_base_price_percentage;
        }else{
            $car_base_price = $this->car_value * ($this->normal_base_price_percentage/100);
            $this->applied_base_percentage = $this->normal_base_price_percentage;
        }

        // formatting output to required decimal places
        $formatted_value = $this->format->format($car_base_price);
        return $formatted_value;
      
    }

    ### FUNCTION TO CALCULATE COMMISSION ###
    private function calculate_commission($base_price) {
        $car_base_price = $base_price;
        $commission_amount = $base_price * ($this->commission_percentage/100);
        
        // formatting output to required decimal places
        $formatted_value = $this->format->format($commission_amount);
        return $formatted_value;
      
    }

    ### FUNCTION TO CALCULATE TAX ###
    private function calculate_tax($base_price) {
        $car_base_price = $base_price;
        $tax_amount = $base_price * ($this->tax_percentage/100);
        
        // formatting output to required decimal places
        $formatted_value = $this->format->format($tax_amount);
        return $formatted_value;
      
    }

    ### FUNCTION TO CALCULATE TOTAL AND INSTALLMENT WISE INSURANCE ###
    public function calculate_insurance() {
        
        $reponse = array();

        $reponse["installments"] = $this->installments;

        $overall_calculation = array();
        $installment_calculation = array();
        $data_headings = array();

        $overall_calculation["car_value"] = $this->format->format($this->car_value);
        $installment_calculation["car_value"] = '';


        // base price; 11% or 13%
        $overall_car_base_price = $this->calculate_base_price();
        $overall_calculation["base_price"] = $overall_car_base_price;

        $installment_base_price = $overall_car_base_price/$this->installments;
        $installment_calculation["base_price"] = $this->format->format($installment_base_price);

        // commission amount; 17% of base price
        $overall_commission_amount = $this->calculate_commission($overall_car_base_price);
        $overall_calculation["commission_amount"] = $overall_commission_amount;

        $installment_commission_amount = $overall_commission_amount/$this->installments;
        $installment_calculation["commission_amount"] = $this->format->format($installment_commission_amount);


        // tax amount; percentage of base price
        $overall_tax_amount = $this->calculate_tax($overall_car_base_price);
        $overall_calculation["tax_amount"] = $overall_tax_amount;

        $installment_tax_amount = $overall_tax_amount/$this->installments;
        $installment_calculation["tax_amount"] = $this->format->format($installment_tax_amount);

        // total insurance cost
        $total_cost = $overall_car_base_price + $overall_commission_amount + $overall_tax_amount;
        $overall_calculation["total_cost"] = $this->format->format($total_cost);

        $installment_total_cost = $total_cost/$this->installments;
        $installment_calculation["total_cost"] = $this->format->format($installment_total_cost);

        ### Overall Calculations in Response
        $reponse["overall_data"] = $overall_calculation;

        ### Installment wise Calculations in Response
        for($i = 1; $i<= $this->installments; $i++){
            $reponse["installment_".$i] = $installment_calculation;
        }

        ### Headings in Response
        $data_headings["car_value"] = "Value";
        $data_headings["base_price"] = "Base Premium (".$this->applied_base_percentage."%)";
        $data_headings["commission_amount"] = "Commission (17%)";
        $data_headings["tax_amount"] = "Tax (".$this->tax_percentage."%)";
        $data_headings["total_cost"] = "Total Cost";
        $reponse["headings"] = $data_headings;
        
        return $reponse;
      
    }


  }