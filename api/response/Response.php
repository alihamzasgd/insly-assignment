<?php
    
class Response {
   
    // Car Insurance Properties
    private $success_code;
    private $error_code;
 
    // Constructor with DB
    public function __construct() {
        $this->success_code = 200;
        $this->error_code = 400;
    }


    public function send_response($data){
        // Headers
        header('Access-Control-Allow-Origin: *');
        header('Content-Type: application/json');
        header('Access-Control-Allow-Methods: POST');
        header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');
        echo json_encode($data); 

    }

}