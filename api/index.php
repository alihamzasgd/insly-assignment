<?php

// Connect to database
require_once("calculator/Calculator.php");
require_once("response/Response.php");

$request_method=$_SERVER["REQUEST_METHOD"];

$calculator = new Calculator();
$response = new Response();

switch($request_method)
	{
		case 'POST':
			// Retrive Request data and set in class object
			
            $data = json_decode(file_get_contents("php://input"));
            $calculator->set_car_value($data->car_value);
            $calculator->set_tax_percentage($data->tax_percentage);
            $calculator->set_installments($data->installments);
            $calculator->set_user_time($data->user_time);
            $calculator->set_user_day($data->user_day);
           
            $response_data = $calculator->calculate_insurance();
            $response->send_response($response_data);
			break;
        
        default:
			// Invalid Request Method
			header("HTTP/1.0 405 Method Not Allowed");
			break;
    }