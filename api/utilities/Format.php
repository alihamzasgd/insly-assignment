<?php

class Format {

    private $decimals;
    // Constructor with DB
    public function __construct() {
    
        $this->decimals = 2;
    }
    
    public function format($number){
        return number_format((float)$number, $this->decimals, '.', '');
    }

}