# Developer Test - Task 2 (Calculator)
This is the 2nd task of the assignment about building a car insurance calculator.

## App Info
Following is a web application build using Bootstrap, Jquery and Javascript for front-end and business logic in Vanilla php following OOP practices. 

### Development Technology Stack
•	Vanilla PHP
•	JavaScript/JQuery

### Features
An interface has been provided where user is supposed to enter required parameters for car insurance calculation. Bootstrap has been used for the front-end view development. Custom css styling has also been used that can be found under the assets folder. 

Form validation is done through JavaScript. User is guided throughout the input process about error related to each input. JQuery Ajax request has been used to handle the flow of data without refreshing the browser page. JavaScript helper functions are defined to manipulated JSON data to the front-end.

All the business logic has been implemented in the PHP following OOP practices and guidelines. .htaccess file is included to prettify PHP end-point URLs. 

Interface of the application and the business logic are loosely coupled which means the business logic implementation can be used with any front-end technology.

### Server Requirements
PHP version 5.6 or newer is recommended.It should work on 5.3.7 as well, but we strongly advise you NOT to run such old versions of PHP, because of potential security and performance issues, as well as missing features.

### Author
Ali Hamza
