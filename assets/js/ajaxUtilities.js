/**
 * Author: Ali Hamza
 * Following file contains ajax calls, action listeners, data formatter and some utility javascript functions
 */
$(document).ready(function(){

    function validateFormData(){
        var flag = true;
        var car_value = $("#car_value").val();
        var tax_percentage = $("#tax_percentage").val();
        var installments = $("#installments").val()
        var error_str = "";

        if( car_value < 100 || car_value > 100000 || car_value == "" || car_value == null ){
            error_str += "<li>Car value must be between 100 and 100,000.</li>"
            flag = false;
        }

        if( tax_percentage < 0 || tax_percentage > 100 || tax_percentage == "" || tax_percentage == null ){
            error_str += "<li>Tax percentage must be between 0 and 100.</li>"
            flag = false;
        }

        if(installments == "" || installments == null){
            error_str += "<li>Please select number of instalments.</li>"
            flag = false;
        }

        if(flag == false){
            $("#form_validation_error_ul").html(error_str);
            $("#form_validation_error_div").show();
        }else{
            $("#form_validation_error_ul").html("");
            $("#form_validation_error_div").hide();
        }
        
        return flag;
    }

    //function to turn from data to a json object
    function converFormDataToJson(data) {
        var hash;
        var jsonData = {};
        var hashes = data.slice(data.indexOf('?') + 1).split('&');
        for (var i = 0; i < hashes.length; i++) {
            hash = hashes[i].split('=');
            jsonData[hash[0]] = hash[1];
        }

        var user_dt = new Date();
        var user_time = user_dt.getHours();
        var user_day = user_dt.getDay();
        
        jsonData["user_day"] = user_day;
        jsonData["user_time"] = user_time;
        return JSON.stringify(jsonData);
    }

    $('#calculateInsurance').click(function(e){
        e.preventDefault();

        var validation = validateFormData();
        if(validation == true){
            //serialize form data
            var fromData = $('#insurance_form').serialize();
            //pass serialized data to function
            var fromJsonData = converFormDataToJson(fromData);

            var base_url = window.location.href;
            //post with ajax
            $.ajax({
                type:"POST",
                url: base_url+"/api/calculate",
                data: fromJsonData,
                ContentType:"application/json",
                success:function(data){
                    generate_table(data);
                },
                error:function(){
                    alert('Could not be posted');
                }
            });
        }
    });


    // helper function to form html table 
    function generate_table(data){
        var costing_table_body = "";
        var costing_table_headings = "<tr> <th></th> <th> Policy </th>";
        var installments = data.installments;
        
        // headings formation  
        var i;
        var installments_car_value_str = "";
        var installments_base_price_str = "";
        var installments_commission_amount_str = "";
        var installments_tax_amount_str = "";
        var installments_total_cost_str = "";

        for (i = 1; i <= installments; i++) {
            costing_table_headings += "<th>"+i+" Instalment</th>";
            var installment_name = "installment_"+i;

            installments_car_value_str += "<td align='right'>"+data[installment_name].car_value+"</th>";
            installments_base_price_str += "<td align='right'>"+data[installment_name].base_price+"</th>";
            installments_commission_amount_str += "<td align='right'>"+data[installment_name].commission_amount+"</th>";
            installments_tax_amount_str += "<td align='right'>"+data[installment_name].tax_amount+"</th>";
            installments_total_cost_str += "<td align='right'>"+data[installment_name].total_cost+"</th>";
        }
        
        $("#costing_table_headings").html(costing_table_headings);

        var car_value_str = "<tr><td>"+data.headings.car_value+"</td><td align='right' >"
                            +data.overall_data.car_value+"</td>"
                            +installments_car_value_str
                            +"<tr>";
        costing_table_body += car_value_str;

        var base_price_str = "<tr><td>"+data.headings.base_price+"</td><td align='right' >"
                            +data.overall_data.base_price+"</td>"
                            +installments_base_price_str
                            +"<tr>";
        costing_table_body += base_price_str;

        var commission_amount_str = "<tr><td>"+data.headings.commission_amount+"</td><td align='right'>"
                            +data.overall_data.commission_amount+"</td>"
                            +installments_commission_amount_str
                            +"<tr>";
        costing_table_body += commission_amount_str;

        var tax_amount_str = "<tr><td>"+data.headings.tax_amount+"</td><td align='right'>"
                            +data.overall_data.tax_amount+"</td>"
                            +installments_tax_amount_str
                            +"<tr>";
        costing_table_body += tax_amount_str;

        var total_cost_str = "<tr><td><strong>"+data.headings.total_cost+"</strong></td><td align='right'><strong>"
                            +data.overall_data.total_cost+"</strong></td>"
                            +installments_total_cost_str
                            +"<tr>";
        costing_table_body += total_cost_str;

        $("#costing_table_body").html(costing_table_body);

        $("#output_container").show();
    }

});